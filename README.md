# Simple Utilities

Simple package providing basic utilities for "Simple" packages.

## Instructions

[Unity](https://docs.unity3d.com/Manual/CustomPackages.html) provides detailed instructions on importing (or creating) a package from a local source.

1. Clone the project to the desired directory (outside the Unity project).
2. Open the packages window and install a local package, using the `package.json` file.

