﻿using UnityEditor;
using UnityEngine;

namespace Unavia.Utilities
{
    public class UtilitiesMenu : MonoBehaviour
    {
        #region Menu Items
        [MenuItem("Unavia/Utilities/Setup Scene")]
        public static void CreateSceneStructure()
        {
            const string levelManagerName = "Level Manager";

            // Prevent duplicating existing scene setup if already run
            GameObject existingLevelManager = GameObject.Find(levelManagerName);
            if (existingLevelManager)
            {
                EditorUtility.DisplayDialog(
                    "Scene Already Setup",
                    "The scene has already been setup and cannot be reconfigured.",
                    "Ok"
                );
                return;
            }

            // Ensure user intended to setup the scene
            bool proceed = EditorUtility.DisplayDialog(
                "Setup Scene",
                "Are you sure you want to setup this scene?\n\nNOTE: Will cause duplications if already configured.",
                "Confirm",
                "Cancel"
            );
            if (!proceed) return;

            // Configure basic level manager groups
            GameObject levelManagerObject = CreateGameObject(levelManagerName);
            GameObject environmentGroup = CreateGameObject("Environment_GRP", levelManagerObject.transform);
            GameObject objectsGroup = CreateGameObject("Objects_GRP", levelManagerObject.transform);
            GameObject lightingGroup = CreateGameObject("Lighting_GRP", levelManagerObject.transform);
            GameObject audioGroup = CreateGameObject("Audio_GRP", levelManagerObject.transform);
            GameObject effectsGroup = CreateGameObject("Effects_GRP", levelManagerObject.transform);

            // Move directional light to lighting group
            GameObject directionalLight = GameObject.Find("Directional Light");
            if (directionalLight)
            {
                directionalLight.name = "Sun";
                directionalLight.transform.SetParent(lightingGroup.transform);
            }
            else
            {
                Debug.LogWarning("Directional Light not found in Scene");
            }
        }

        /// <summary>
        /// Prevent creating scene structure when any object has been selected
        /// </summary>
        /// <returns>Whether menu option is enabled</returns>
        [MenuItem("Utilities/Setup Scene", true)]
        public static bool ValidateCreateSceneStructure()
        {
            return Selection.activeTransform == null;
        }
        #endregion


        #region Helper Methods
        /// <summary>
        /// Create a basic nested GameObject in the Hiearchy
        /// </summary>
        /// <param name="name">Game object name</param>
        /// <param name="parent">Parent object in Hiearchy</param>
        /// <returns>Created nested GameObject</returns>
        public static GameObject CreateGameObject(string name, Transform parent)
        {
            GameObject group = new GameObject(name);

            if (parent)
            {
                group.transform.SetParent(parent, false);
            }

            return group;
        }

        /// <summary>
        /// Create a basic GameObject in the Hiearchy
        /// </summary>
        /// <param name="name">Game object name</param>
        /// <returns>Created GameObject</returns>
        public static GameObject CreateGameObject(string name)
        {
            return CreateGameObject(name, null);
        }
        #endregion
    }
}

