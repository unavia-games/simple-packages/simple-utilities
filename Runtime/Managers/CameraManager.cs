﻿using UnityEngine;

namespace Unavia.Utilities
{
    public class CameraManager : GameSingleton<CameraManager>
    {
        /// <summary>
        /// Main game camera.
        /// </summary>
        // [Required]
        public Camera MainCamera;
    }
}

