﻿using System;

namespace Unavia.Utilities
{
    [Serializable]
    public class IntVector2
    {
        #region Variables
        public int x;
        public int y;
        #endregion

        public IntVector2(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
}
