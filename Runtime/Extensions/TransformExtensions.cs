﻿using UnityEngine;
using System.Collections;

namespace Unavia.Utilities
{
    public static class TransformExtensions
    {
        /// <summary>
        /// Create a nested GameObject in the hierarchy
        /// </summary>
        /// <param name="parent">Parent transform</param>
        /// <param name="childName">New GameObject name</param>
        /// <returns>Nested GameObject</returns>
        public static GameObject CreateGameObject(this Transform parent, string childName)
        {
            GameObject child = new GameObject(childName);
            child.transform.SetParent(parent);

            return child;
        }

        /// <summary>
        /// Get the normalized direction to a target
        /// </summary>
        /// <param name="origin">Origin transform</param>
        /// <param name="target">Target transform</param>
        /// <returns>Direction to a target</returns>
        public static Vector3 DirectionTo(this Transform origin, Transform target)
        {
            return origin.position.DirectionTo(target.position);
        }

        /// <summary>
        /// Get the rotation from one point to another
        /// </summary>
        /// <param name="origin">Origin transform</param>
        /// <param name="target">Target transform</param>
        /// <returns>Rotation from one point to another</returns>
        public static Quaternion RotationTo(this Transform origin, Transform target) {
            return origin.position.RotationTo(target.position);
        }
    }
}
