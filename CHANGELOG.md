# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## v0.3.0 (2021-Jan-16)

### Added
- Mouse utilities to get "look" point on a plane
- Additional `DirectionTo` and `RotationTo` utilities to `Transform` class (not just `Vector3`)

### Fixed
- Swap the origin and target of the `DirectionTo` methods
- Included last list items in the `IEnumerable.Random()` extensions methods

### Removed
- `EditorExtensions` class broke as it was causing builds to fail

## v0.2.0 (2020-May-19)

### Added
- Attribute and drawer for requiring an interface (`RequireInterface`)
- Float extensions - `Map`, `MapFromPercent`, `MapTopPercent`
- Color extensions - `Darken`
- Pool system using interfaces (manager, pool config)

### Removed
- `ReadOnly` attribute (in favour of Odin Inspector)
- Unnecessary (incomplete) camera utilities (`BasicFollowCamera`)
- Incomplete FPS profiler
- Unnecessary camera manager

## v0.1.0 (2020-May-15)

### Added
- `ReadOnly` properties drawer
- Utilities menu with scene setup options
- Basic 3D follow camera
- Simple FPS profiler indicator
- Quite a few extension methods
	- Arrays - `Shuffle`
	- Game Object - `ClearChildren`, `CreateGameObject`
	- IEnumerable - `Random`, `RandomByWeight`
	- Layer Mask - `Invert`, `ContainsLayer`
	- Transform - `CreateGameObject`
	- Vector3 - `DirectionTo`, `ClampedDirectionTo`, `RotationTo`, `ClampedRotationTo`, `PointAlongLine`
- Some common singleton managers
	- Audio manager
	- Camera manager
	- Temporary objects manager
- Several utility classes/types
	- Monobehaviour extension (wait methods, etc)
	- Game singleton class
	- `IntVector2` struct
- UI extension to center on the window

